import 'package:intl/intl.dart';

num epochNow() => epoch(DateTime.now());

DateTime midnight(DateTime datetime) => DateTime(datetime.year, datetime.month, datetime.day);

num epoch(DateTime datetime) => num.parse((datetime.millisecondsSinceEpoch / 1000).toStringAsFixed(0));

DateTime datetime(num epoch) => DateTime.fromMillisecondsSinceEpoch(epoch * 1000);

String formatEpoch(num epoch, String format) {
  return new DateFormat(format).format(datetime(epoch));
}