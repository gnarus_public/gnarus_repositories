import 'package:gnarus_repositories/model/model.dart';

abstract class Repository<T extends Model> {
  Future<List<T>> findAll();

  Stream<List<T>> findAll$();

  Future<T> findById(String id);

  Stream<T> findById$(String id);

  Future<List<T>> findByField(String name, Object value);

  Stream<List<T>> findByField$(String name, Object value);

  Future<List<T>> findByFields(Map<String, Object> fields);

  Stream<List<T>> findByFields$(Map<String, Object> fields);

  Future<T> findOneByField(String name, Object value);

  Stream<T> findOneByField$(String name, Object value);

  Future<T> findOneByFields(Map<String, Object> fields);

  Stream<T> findOneByFields$(Map<String, Object> fields);

  Future<T> save(T model);

  Future<void> saveAll(List<T> models);

  Future<void> delete(String id);

  Future<void> deleteAll(List<String> ids);
}
