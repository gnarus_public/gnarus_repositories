import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:gnarus_repositories/model/model.dart';
import 'package:gnarus_repositories/util/date_util.dart';
import 'package:meta/meta.dart';

import '../repository.dart';

abstract class FirebaseRepository<T extends Model> extends Repository<T> {
  final String _path;
  final Firestore _firestore;

  FirebaseRepository(this._path, this._firestore);

  T _firstOrNull(List<T> models) => (models == null) || (models.isEmpty) ? null : models[0];

  Stream<T> _takeFirst(Stream<List<T>> stream) =>
      stream.where((models) => models != null && models.isNotEmpty).map((models) => models[0]);

  @protected
  WriteBatch $batch() => this._firestore.batch();

  @protected
  CollectionReference $collection() => this._firestore.collection(_path);

  @protected
  $document(String id) {
    ArgumentError.checkNotNull(id, "id");
    return $collection().document(id);
  }

  @protected
  T $getInstance();

  @protected
  T $getModel(DocumentSnapshot documentSnapshot) {
    if ((documentSnapshot == null) || (!documentSnapshot.exists)) {
      return null;
    }

    T model = $getInstance();
    model.fill(documentSnapshot.data);
    model.id = documentSnapshot.documentID;
    return model;
  }

  @protected
  List $getModels(QuerySnapshot querySnapshot) =>
      querySnapshot.documents.map((snapshot) => $getModel(snapshot)).toList();

  @protected
  void $fillRequiredFields(Model model) {
    var now = epochNow();

    if (model.created == null) {
      model.created = now;
    }

    if (model.updated == null) {
      model.updated = now;
    }
  }

  @protected
  Query $query(Map<String, Object> fields) {
    ArgumentError.checkNotNull(fields);

    if (fields.isEmpty) {
      ArgumentError.value(fields, "fields", "Invalid argument, cannot be empty");
    }

    Query query;
    fields.forEach((name, value) {
      if (["", null].contains(name)) {
        ArgumentError.value(name, "name");
      }

      if (query == null) {
        query = $collection().where(name, isEqualTo: value);
      } else {
        query = query.where(name, isEqualTo: value);
      }
    });

    return query;
  }

  @override
  Future<List<T>> findAll() async => $getModels(await $collection().getDocuments());

  @override
  Stream<List<T>> findAll$() => $collection().snapshots().map((snapshots) => $getModels(snapshots));

  @override
  Future<T> findById(String id) async => $getModel(await $document(id).get());

  @override
  Stream<T> findById$(String id) => $document(id).snapshots().map((snapshot) => $getModel(snapshot));

  @override
  Future<List<T>> findByField(String name, Object value) => findByFields({name: value});

  @override
  Stream<List<T>> findByField$(String name, Object value) => findByFields$({name: value});

  @override
  Future<List<T>> findByFields(Map<String, Object> fields) async => $getModels(await $query(fields).getDocuments());

  @override
  Stream<List<T>> findByFields$(Map<String, Object> fields) =>
      $query(fields).snapshots().map((snapshots) => $getModels(snapshots));

  @override
  Future<T> findOneByField(String name, Object value) => findOneByFields({name: value});

  @override
  Stream<T> findOneByField$(String name, Object value) => findOneByFields$({name: value});

  @override
  Future<T> findOneByFields(Map<String, Object> fields) async => _firstOrNull(await findByFields(fields));

  @override
  Stream<T> findOneByFields$(Map<String, Object> fields) => _takeFirst(findByFields$(fields));

  @override
  Future<T> save(T model) async {
    ArgumentError.checkNotNull(model, "model");

    if (model.id == null) {
      var ref = await $collection().add(model.fields);
      model.id = ref.documentID;
    } else {
      await $collection().document(model.id).setData(model.fields);
    }

    return findById(model.id);
  }

  @override
  Future<void> saveAll(List<T> models) async {
    throw StateError("Not implemented, firebase does not support batch add operation");
  }

  @override
  Future<void> delete(String id) async {
    ArgumentError.checkNotNull(id, "id");

    await $collection().document(id).delete();
  }

  @override
  Future<void> deleteAll(List<String> ids) async {
    ArgumentError.checkNotNull(ids, "ids");

    if (ids.isEmpty) {
      return;
    }

    if (ids.length > 500) {
      // https://firebase.google.com/docs/firestore/manage-data/transactions
      // Each transaction or batch of writes can write to a maximum of 500 documents.
      ArgumentError.value(ids.length, "ids", "Invalid size, must be 500 or less");
    }

    var batch = $batch();

    for (String id in ids) {
      ArgumentError.checkNotNull(id, "id");
      batch.delete(this.$collection().document(id));
    }

    await batch.commit();
  }
}
