class Model {
  final Map<String, dynamic> fields = Map();

  String get id => fields["id"];

  int get created => fields["created"];

  int get updated => fields["updated"];

  int get deleted => fields["deleted"];

  set id(String value) => fields["id"] = value;

  set created(int value) => fields["created"] = value;

  set updated(int value) => fields["updated"] = value;

  set deleted(int value) => fields["deleted"] = value;

  fill(Map<String, dynamic> map) {
    fields.addAll(map);
  }
}
